<table class="table table-hover">
    <thead>
    <tr>
        <th scope="col">Дата добавления</th>
        <th scope="col">Favicon</th>
        <th scope="col">URL</th>
        <th scope="col">Заголовок страницы</th>
    </tr>
    </thead>
    <tbody>

    @foreach($bookmarks as $bookmark)
        <tr class='clickable-row' data-href='{{ route('bookmarks.show', ['id' => $bookmark->id]) }}'>
            <td scope="row">{{ $bookmark->created_at->format('d.m.Y') }}</td>
            <td>@if($bookmark->favicon)<img width="16px" src="{{ $bookmark->favicon }}" alt="">@endif</td>
            <td>{{ $bookmark->url }}</td>
            <td>{{ $bookmark->title }}</td>
        </tr>
    @endforeach

    </tbody>
</table>