<div class="modal fade" id="deleteModalCenter" tabindex="-1" role="dialog" aria-labelledby="deleteModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalCenterTitle">Удаление закладки</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('bookmarks.destroy', $bookmark->id) }}" id="deleteBookmark">
                @method('DELETE')
                <div class="modal-body">
                    <div class="alert alert-warning" role="alert">
                        Для удаления закладки необходимо ввести пароль, который был установлен при её добавлении.
                    </div>
                    <div style="display: none" class="alert alert-danger err-msg" role="alert"></div>
                    <input required id="pass" type="password" class="form-control" placeholder="Введите пароль для удаления">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                    <button type="submit" class="btn btn-danger">Удалить закладку</button>
                </div>
            </form>
        </div>
    </div>
</div>