@extends('master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="pt-4">
                <div class="shadow-lg p-3 mb-5 bg-white rounded">
                    <h1>Список всех закладок</h1>
                </div>
            </div>
        </div>

        <div class="col-lg-12 pb-4">
            <a href="{{ route('bookmarks.create') }}" class="btn btn-primary btn-sm active" role="button" aria-pressed="true">
                Добавить новую закладку
            </a>
        </div>

        @yield('bookmarks')
    </div>
@endsection
