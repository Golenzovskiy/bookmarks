@extends('master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="pt-4">
                <div class="shadow-lg p-3 mb-5 bg-white rounded">
                    <h1>Добавление новой закладки</h1>
                </div>
            </div>
        </div>

        <div class="col-lg-12 pb-4">
            <a href="{{ route('bookmarks.index') }}"
               class="btn btn-secondary btn-sm active" role="button" aria-pressed="true">
                Вернуться к списку закладок
            </a>
        </div>

        <div class="col-lg-12">
            <div class="shadow-sm p-3 mb-5 bg-white rounded">
                <form action="{{ route('bookmarks.store') }}" method="post" id="createBookmark">
                    <div class="form-group row">
                        <label for="url" class="col-sm-2 col-form-label">URL закладки</label>
                        <div class="col-sm-10">
                            <input required type="text" class="form-control" id="url" placeholder="URL страницы">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="pass" class="col-sm-2 col-form-label">Пароль для удаления</label>
                        <div class="col-sm-10">
                            <input required type="password" class="form-control" id="pass" placeholder="Пароль удаления">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Добавить закладку</button>
                </form>

                <div class="pt-4">
                    <div class="alert alert-danger err-msg" style="display: none" role="alert"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
