@extends('pages.index')

@section('bookmarks')
@if($bookmarks->count() > 0)
    <div class="col-lg-12">
        <div class="shadow-sm p-3 mb-5 bg-white rounded table-responsive">
            @include('pages.search.form')

            @include('includes.bookmarkTable')

            {{ $bookmarks->links() }}
        </div>
    </div>
@endif
@endsection