@extends('master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="pt-4">
                <div class="shadow-lg p-3 mb-5 bg-white rounded">
                    <h1>Информация о закладке</h1>
                </div>
            </div>
        </div>

        <div class="col-lg-12 pb-4">
            <a href="{{ route('bookmarks.index') }}"
               class="btn btn-secondary btn-sm active" role="button" aria-pressed="true">
                Вернуться к списку закладок
            </a>

            <a id="js-delete"
               href="#deleteModalCenter"
               data-toggle="modal"
               class="btn btn-danger btn-sm active float-right" role="button" aria-pressed="true">
                Удалить закладку
            </a>
        </div>

        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>{{ $bookmark->title ?? '' }}</strong>
                </div>
                <div class="card-body">
                    <p class="card-title">
                        @if($bookmark->favicon)
                            <img width="16px" src="{{ $bookmark->favicon }}" alt="">
                        @endif
                        <a class="pl-2" href="{{ $bookmark->url }}">{{ $bookmark->url }}</a></p>
                    @if($bookmark->meta_description)
                        <p class="card-text">{{ $bookmark->meta_description }}</p>
                    @endif
                    @if($bookmark->meta_keywords)
                        @foreach(explode(',', $bookmark->meta_keywords) as $keyword)
                            <button type="button" class="btn btn-secondary btn-sm">{{$keyword}}</button>
                        @endforeach
                    @endif
                </div>
                <div class="card-footer text-muted">
                    {{ $bookmark->created_at }}
                </div>
            </div>
        </div>
    </div>
    @include('modal.delete', ['bookmark->id' => $bookmark->id])
@endsection
