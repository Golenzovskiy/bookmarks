@extends('pages.index')

@section('bookmarks')
    <div class="col-lg-12">
        <div class="shadow-sm p-3 mb-5 bg-white rounded table-responsive">
            @include('pages.search.form', ['search' => $searchQuery])

            @if($searchResult->count() > 0)
                <div class="alert alert-secondary" role="alert">
                    По запросу "<strong>{{ $searchQuery }}</strong>" найдено {{ $searchResult->total() }}
                    {{ \Illuminate\Support\Facades\Lang::choice('результат|результата|результов',
                    $searchResult->total(), [], 'ru') }}.
                </div>

                @include('includes.bookmarkTable', ['bookmarks' => $searchResult])
                {{ $searchResult->links() }}
            @else
                <div class="alert alert-secondary" role="alert">
                    По запросу "<strong>{{ $searchQuery }}</strong>" ничего не найдено.
                </div>
            @endif
        </div>
    </div>
@endsection