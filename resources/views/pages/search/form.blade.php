<div class="pt-2 pb-4">
    <form method="get">
        <div class="form-row">
            <div class="col-lg-12">
                <label class="sr-only" for="search">Search</label>
                <input required type="text"
                       class="form-control mb-2"
                       id="search" name="query"
                       placeholder="Поиск по закладкам"
                       value="{{ $searchQuery ?? '' }}">
            </div>
            <div class="col-lg-12">
                <input class="btn btn-primary" type="submit" value="Поиск">
                @if(isset($searchQuery))
                    <a class="btn btn-primary btn-outline-danger" href="{{ route('bookmarks.index') }}">сбросить</a>
                @endif
            </div>
        </div>
    </form>
</div>