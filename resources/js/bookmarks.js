bookmarkApp = {
    deleteBookmark: function (action, deletionCode) {
        axios.delete(action, {params: {code: deletionCode}})
            .then(function (response) {
                if (response.data.status === 'success') {
                    window.location = response.data.redirect;
                } else if (response.data.status === 'error') {
                    $('.err-msg').show().html(response.data.message);
                }
            })
            .catch(function (error) {
                $('.err-msg').show().html(error.message);
                console.log(error);
            });

    },

    createBookmark: function (action, bookmarkUrl, deletionCode) {
        axios.post(action, {
                url: bookmarkUrl,
                pass: deletionCode
            },
            {
                validateStatus: function (status) {
                    return status < 500;
                }
            })
            .then(function (response) {
                if (response.data.status === 'success') {
                    window.location = response.data.redirect;
                } else if (response.data.status === 'error') {
                    $('.err-msg').show().html(response.data.message);
                } else {
                    console.log(response);
                    $('.err-msg').show().html(response.data.errors.url);
                }
            })
            .catch(function (error) {
                $('.err-msg').show().html(error.message);
                console.log(error);
            });
    }
};

$(function() {
    $(".clickable-row").click(function () {
        window.location = $(this).data("href");
    });

    $("#deleteBookmark").submit(function (e) {
        var action = $(this).attr('action'),
            deletionCode = $('#pass').val();
        bookmarkApp.deleteBookmark(action, deletionCode);
        e.preventDefault();
    });

    $("#createBookmark").submit(function (e) {
        var action = $(this).attr('action'),
            bookmarkUrl = $('#url').val(),
            deletionCode = $('#pass').val();
        bookmarkApp.createBookmark(action, bookmarkUrl, deletionCode);
        e.preventDefault();
    });
});
