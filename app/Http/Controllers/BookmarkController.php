<?php

namespace Bookmarks\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Bookmarks\Models\Bookmark;
use Bookmarks\Models\DeletionCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookmarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($query = $request->input('query')) {
            $result = Bookmark::search($query)->paginate(2);

            return view('pages.search.result', [
                'searchQuery'   => $query,
                'searchResult'  => $result,
                'searchTotal'   => $result->total()
            ]);
        } else {
            $bookmarks = Bookmark::select('id', 'created_at', 'url', 'favicon', 'title')
                ->orderBy('created_at', 'desc')
                ->paginate(5);

            return view('pages.bookmarks', [
                'bookmarks' => $bookmarks
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('pages.create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'url'   => 'required|active_url',
            'pass'  => 'required'
        ]);

        $bookmark = new Bookmark();
        if ($bookmark->isExist($request->url)) {
            return response()->json([
                'message'   => 'Закладка с таким url уже существует.',
                'status'    => 'error',
                'redirect'  => ''
            ]);
        } else {
            try {
                DB::beginTransaction();
                $bookmark->createByUrl($request->url);
                $code = new DeletionCode(['code' => Hash::make($request->pass)]);
                $bookmark->deletionCode()->save($code);
                DB::commit();

                return response()->json([
                    'message'   => '',
                    'status'    => 'success',
                    'redirect'  => route('bookmarks.show', $bookmark->id)
                ]);
            } catch (\Exception $e) {
                DB::rollBack();

                return response()->json([
                    'message'   => 'Не удалось добавить закладку.',
                    'status'    => 'error',
                    'redirect'  => ''
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $bookmark = Bookmark::findOrFail($id);

        return view('pages.bookmark', [
            'bookmark' => $bookmark
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $id
     * @return string
     */
    public function destroy(Request $request, $id)
    {
        $bookmark = Bookmark::find($id);
        $deletionCode = $bookmark->deletionCode;
        
        if (Hash::check($request->code, $deletionCode->code)) {
            try {
                DB::beginTransaction();
                $bookmark->delete();
                $deletionCode->delete();
                DB::commit();

                return response()->json([
                    'message'   => '',
                    'status'    => 'success',
                    'redirect'  => route('bookmarks.index')
                ]);
            } catch (\Exception $e) {
                DB::rollBack();

                return response()->json([
                    'message'   => 'Не удалось удалить закладку.',
                    'status'    => 'error',
                    'redirect'  => ''
                ], 500);
            }
        } else {
            return response()->json([
                'message'   => 'Введен некорректный пароль.',
                'status'    => 'error',
                'redirect'  => ''
            ]);
        }
    }
}
