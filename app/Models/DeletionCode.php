<?php

namespace Bookmarks\Models;

use Illuminate\Database\Eloquent\Model;

class DeletionCode extends Model
{
    public $timestamps = false;

    protected $fillable = ['code'];
}
