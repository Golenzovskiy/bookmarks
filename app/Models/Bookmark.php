<?php

namespace Bookmarks\Models;

use Bookmarks\Helpers\UrlCorrector;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Laravel\Scout\Searchable;
use QL\QueryList;

class Bookmark extends Model
{
    use Searchable;

    protected $fillable = ['url', 'title', 'favicon', 'meta_description', 'meta_keywords'];

    public static function boot()
    {
        parent::boot();

        self::deleted(function ($model) {
            Storage::delete('public/' . $model->favicon);
        });
    }

    public function deletionCode()
    {
        return $this->hasOne('Bookmarks\Models\DeletionCode');
    }

    /**
     * Check existence by URL.
     *
     * @param string $url
     * @return bool
     */
    public function isExist(string $url): bool
    {
        return $this->where('url', $url)->count() > 0 ? true : false;
    }

    /**
     * Create model by Url.
     *
     * @param string $url
     */
    public function createByUrl(string $url)
    {
        $ql = QueryList::get($url);
        $hrefIco = $ql->find('link[rel*="icon"]')->href;

        $this->fill([
            'url' => $url,
            'title' => $ql->find('title')->text(),
            'favicon' => $this->getFavicon($url, $hrefIco),
            'meta_description' => $ql->find('meta[name=description]')->content,
            'meta_keywords' => $ql->find('meta[name=keywords]')->content
        ]);
        $this->save();
    }

    /**
     * @param string $url
     * @param string $hrefIco
     * @return string
     */
    protected function getFavicon(string $url, string $hrefIco)
    {
        if (!empty($hrefIco)) {
            $urlCorrector = new UrlCorrector;
            $urlCorrector->setAbsoluteComponents($url);
            $faviconUrl = $urlCorrector->relativeToAbsolute($hrefIco);

            $faviconExt = explode(".", $faviconUrl);
            $faviconExt = end($faviconExt);
            $faviconName = md5($url) . ".$faviconExt";
            Storage::put('public/favicons/' . $faviconName, file_get_contents($faviconUrl));
            return '/favicons/' . $faviconName;
        } else {
            return $hrefIco;
        }
    }
}
