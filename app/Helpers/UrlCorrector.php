<?php
declare(strict_types=1);

/**
 * @author Golenzovskiy Stanislav <golenzovskiy@gmail.com>
 * @copyright Copyright (c) 2018, bookmarks
 */

namespace Bookmarks\Helpers;

class UrlCorrector
{
    protected $url;
    protected $host;
    protected $scheme;
    protected $domain;

    protected function setUrl($url): void
    {
        $this->url = $url;
    }

    protected function setScheme(): void
    {
        $this->scheme = parse_url($this->url, PHP_URL_SCHEME);
    }

    protected function setHost(): void
    {
        $this->host = parse_url($this->url, PHP_URL_HOST);
    }


    protected function setDomain(): void
    {
        $this->domain = "$this->scheme://$this->host";
    }

    public function setAbsoluteComponents(string $url)
    {
        $this->setUrl($url);
        $this->setScheme();
        $this->setHost();
        $this->setDomain();
    }

    public function relativeToAbsolute(string $href): string
    {
        if (parse_url($href, PHP_URL_SCHEME)) {
            return $href;
        }

        if (substr($href, 0, 1) === '/') {
            return $this->domain . $href;
        } elseif (substr($href, 0, 3) === 'www') {
            return "$this->scheme://$href";
        } else {
           return (strripos($href, '/')) ? "$this->scheme://$href" : "$this->domain/$href";
        }
    }
}